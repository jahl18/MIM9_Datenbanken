import express from 'express';
import { client } from './server.js';

const router = express.Router();

// Lege ein neues Set an Büchern an, oder füge ein Buch zu einem Set hinzu
router.post('/set', (req, res) => {

    // Sie bekommen im Body setName und book übergeben

    // Legen Sie ein neues Set an Büchern an
    // Wenn die Aktion erfolgreich war, geben Sie den Status 200, eine entsprechende message und den namen des Sets und des Buches zurück
    // Fangen Sie einen möglichen Fehler ab. Gebe dabei den status 500 und die Fehlermeldung zurück

});

// Ein Buch von einem Set löschen
router.delete('/:setName/:book', (req, res) => {


    // Löschen Sie das übergebene Buch aus dem Set
    // Wenn die Aktion erfolgreich war, geben Sie den Status 200, eine entsprechende message und den namen des Sets und des Buches zurück
    // Fangen Sie einen möglichen Fehler ab. Gebe dabei den status 500 und die Fehlermeldung zurück

});

// Alle Sets an Büchern abrufen
router.get('/allSets', (req, res) => {

    // Geben Sie alle Sets an Büchern zurück
    // Wenn die Aktion erfolgreich war, geben Sie zusätzlich den Status 200 zurück
    // Fangen Sie einen möglichen Fehler ab. Gebe dabei den status 500 und die Fehlermeldung zurück

});

// Alle Bücher in einem Set abrufen
router.get('/:setName', (req, res) => {

    // Geben Sie alle Bücher in einem Set zurück
    // Wenn die Aktion erfolgreich war, geben Sie zusätzlich den Status 200 zurück
    // Fangen Sie einen möglichen Fehler ab. Gebe dabei den status 500 und die Fehlermeldung zurück

});


export default router