import express from 'express';
import redis from 'redis';
import cors from 'cors';

import bookRouter from './bookRouter.js'
import userRouter from './userRouter.js'

const PORT = process.env.PORT || 5001;
const app = express();

// Redis muss auf dem defaultPort laufen
export const client = redis.createClient();

client.on('connect', () => {
    console.log('Connected to Redis...');
});

client.on('error', (err) => {
    console.error('Error connecting to Redis:', err);
});

await client.connect();

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));


app.use('/book', bookRouter)
app.use('/user', userRouter)


app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
