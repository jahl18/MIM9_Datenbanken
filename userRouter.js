import express from 'express';
import {client} from './server.js';
import axios from "axios";

const router = express.Router();

// Erstellen Sie eine Funktion die den cache zurückgibt, oder setzt. Dieser Funktion wird ein Key und Callback übergeben
// Nutzen sie diese Funktion, um sowohl für die beiden folgenden Routen einen Cache zu erstellen und zu benutzen


// Gebe einen bestimmten oder alle User zurück
router.get('/', async (req, res) => {

    // Cachen Sie die User für 10 Sekunden in Redis
    // Wenn die User in Redis vorhanden sind, geben Sie diese zurück
    // Wenn die User nicht in Redis vorhanden sind, holen Sie diese von der API

    const {data} = await axios.get(`https://jsonplaceholder.typicode.com/users`).catch((err) => {
        res.status(500).send({error: err.message});
    })

    res.json(data)
})

// Gebe einen bestimmten User zurück
router.get('/:id', async (req, res) => {

    // Cachen Sie den User für 10 Sekunden in Redis
    // Wenn der User in Redis vorhanden ist, geben Sie diesen zurück
    // Wenn der User nicht in Redis vorhanden ist, holen Sie diesen von der API

    const {data} = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}`).catch((err) => {
        res.status(500).send({error: err.message});
    })

    res.json(data)
})

export default router
