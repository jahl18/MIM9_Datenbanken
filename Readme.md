# Vorbereitung

Für die folgenden Übungsaufgaben wird Node.js und Docker benötigt. 

Die Installationsdatei für Node.js finden Sie unter folgendem Link:

https://nodejs.org/en/download

Eine Anleitung zur Installation von Docker finden Sie unter folgendem Link:

https://docs.docker.com/engine/install/

---

Stellen Sie sicher, dass diese installiert sind und der Docker-Container für Redis läuft.
Diesen erstellen sie mit dem folgenden Befehl:
    
```bash
docker run -d --name redisDatabase -v /Users/jeromehelzel/Desktop/redis_data:/data -p 6379:6379 redis
```
**Der angegebene Pfad muss an dieser Stelle durch ihren Pfad ersetzt werden.**

<br>

Starten des Docker-Containers:

```bash
docker start redisDatabase
```

---

## Installation der benötigten Pakete

In diesem Projekt werden zudem noch weitere Pakete benötigt. Diese können mit folgendem Befehl installiert werden:

```bash
npm install
```
Um den Server zu starten führen Sie folgenden Befehl aus:

```bash
npm start
```

Die Syntax der Redis-Befehle in JavaScript, sind weitestgehend intuitiv.
Die Syntax für diese Befehle finden Sie in den folgenden Dokumentationen:

https://www.npmjs.com/package/redis

https://redis.io/docs/clients/nodejs/

---

# Übungsaufgaben

## Fingerübung
**Du bist ein Entwickler für einen Online-Shop und arbeitest mit Redis, einer leistungsstarken In-Memory-Datenbank. Deine Aufgabe ist es, bestimmte 
Funktionalitäten für den Warenkorb des Online-Shops zu implementieren. Verwende die eben vorgestellten Redis-Befehle, um die folgenden Aufgaben zu 
lösen.:**

1. Speichere den Preis für das Produkt mit der ID "123" im Schlüssel "product:123:price".
2. Überprüfe, ob der Preis für das Produkt mit der ID "123" gespeichert ist.
3. Wenn der Preis vorhanden ist, hole ihn.
4. Lösche den Preis für das Produkt mit der ID "123".
5. Überprüfe erneut, ob der Preis für das Produkt mit der ID "123" noch vorhanden ist.
6. Speichere eine Liste von drei Produkt-IDs ("123", "456", "789") im Schlüssel "cart:products".
7. Füge weitere zwei Produkt-IDs ("999", "888") zur Liste hinzu, jedoch an das Ende der Liste.
8. Hole die ersten drei Produkt-IDs aus der Liste.
9. Entferne das erste Element aus der Liste.
10. Entferne das letzte Element aus der Liste.
11. Speichere die Menge der Produkt-IDs ("123", "456", "789") im Schlüssel "cart:product_ids".
12. Überprüfe, ob die Produkt-ID "123" in der Menge enthalten ist.
13. Hole alle Produkt-IDs aus der Menge.
14. Entferne die Produkt-ID "789" aus der Menge.
15. Speichere die Eigenschaft "color" mit dem Wert "blue" für das Produkt mit der ID "456" im Schlüssel "product:456:properties".
16. Überprüfe, ob die Eigenschaft "color" für das Produkt mit der ID "456" gespeichert ist.
17. Hole den Wert der Eigenschaft "color" für das Produkt mit der ID "456".
18. Hole alle Eigenschaften (Schlüssel-Wert-Paare) für das Produkt mit der ID "456".
19. Lösche die Eigenschaft "color" für das Produkt mit der ID "456".
20. Lösche alle Schlüssel und deren Werte in der Redis-Datenbank.

## Aufgabe 1: Einfache Redis-Operationen
In dieser Aufgabe sollen Sie sich mit den grundlegenden Operationen von Redis vertraut machen.
Gucken sie sich dazu die Datei bookRouter an. Diese Datei stellt die grundlegenden Routen für die API bereit.
Die Aufgabe besteht nun darin die Lücken in den Routen zu füllen. Die Kommentare in den einzelnen Routen sagen ihnen, was zu tun ist.

## Aufgabe 2: Redis als Cache
In dieser Aufgabe sollen Sie sich mit der Verwendung von Redis als Cache vertraut machen.
Schauen Sie sich dazu die Datei bookRouter an. Diese Datei stellt die grundlegenden Routen für die API bereit.
Hier ist der Aufbau gleich, der bei der ersten Aufgabe. Sie finden im Code alle weiteren Informationen zur genauen Aufgabe in der Form von 
Kommentaren.

---

# Grundlegende Redis Befehle

## Strings + Allgemeine Befehle

### SET

Der Befehl `SET` speichert einen Wert unter einem Schlüssel in Redis.

Syntax: `SET key value`

Beispiel:

```
SET mykey "Hello Redis"
```

### GET

Der Befehl `GET` gibt den Wert eines Schlüssels in Redis zurück. Dieser Befehl funktioniert nur für Strings

Syntax: `GET key`

Beispiel:

```
GET mykey
```

### EXISTS

Der Befehl `EXISTS` prüft, ob ein Schlüssel in Redis existiert.

Syntax: `EXISTS key`

Beispiel:

```
SET mykey "Hello Redis"
EXISTS mykey
```

Das obige Beispiel gibt `1` zurück, wenn der Schlüssel `mykey` in Redis vorhanden ist, andernfalls gibt es `0` zurück.

### KEYS

Der Befehl `KEYS` gibt eine Liste aller Schlüssel zurück, die einem bestimmten Muster entsprechen.

Syntax: `KEYS pattern`

Beispiel:

```
SET name "Max"
SET age 25
SET location "Berlin"
KEYS *name*
```

Das obige Beispiel gibt alle Schlüssel zurück, die das Wort "name" im Schlüssel enthalten, also `"name"`.

Wenn Sie jedoch den Befehl `KEYS *` ausführen, werden alle Schlüssel im Redis-Speicher zurückgegeben, was bei großen Datenmengen sehr zeitaufwändig sein kann und die Leistung beeinträchtigen kann. Daher wird empfohlen die Muster als Argument möglichst spezifisch zu halten.

### INCR

Der Befehl `INCR` inkrementiert den Wert eines Schlüssels in Redis um 1.

Syntax: `INCR key`

Beispiel:

```
SET counter 0
INCR counter
```

### INCRBY

Der Befehl `INCRBY` inkrementiert den Wert eines Schlüssels in Redis um einen bestimmten Wert.

Syntax: `INCRBY key increment`

Beispiel:

```
SET counter 0
INCRBY counter 5

```

Das obige Beispiel inkrementiert den Wert des Schlüssels `counter` in Redis um `5`. Wenn der Schlüssel `counter` noch nicht existiert, wird er mit dem Wert `0` erstellt und anschließend inkrementiert.

### DECR

Der Befehl `DECR` dekrementiert den Wert eines Schlüssels in Redis um 1.

Syntax: `DECR key`

Beispiel:

```
SET counter 10
DECR counter
```

### DECRBY

Der Befehl `DECRBY` dekrementiert den Wert eines Schlüssels in Redis um einen bestimmten Wert.

Syntax: `DECRBY key decrement`

Beispiel:

```
SET counter 10
DECRBY counter 5

```

Das obige Beispiel dekrementiert den Wert des Schlüssels `counter` in Redis um `5`. Wenn der Schlüssel `counter` noch nicht existiert, wird er mit dem Wert `0` erstellt und anschließend dekrementiert.

### SETNX

Der Befehl `SETNX` speichert einen Wert unter einem Schlüssel in Redis, wenn dieser Schlüssel noch nicht existiert.

Syntax: `SETNX key value`

Beispiel:

```
SETNX mykey "Hello Redis"

```

Das obige Beispiel speichert den Wert `"Hello Redis"` unter dem Schlüssel `mykey`, wenn dieser Schlüssel noch nicht existiert. Wenn der Schlüssel bereits existiert, hat der Befehl keine Auswirkungen. Der Rückgabewert des Befehls gibt an, ob der Schlüssel erfolgreich gesetzt wurde. Wenn der Schlüssel erfolgreich gesetzt wurde, gibt Redis `1` zurück. Wenn der Schlüssel bereits existierte, gibt Redis `0` zurück.

Dieser Befehl kann nützlich sein, wenn Sie sicherstellen möchten, dass ein Schlüssel nur einmal in Redis gesetzt wird.

### MSET

Der Befehl `MSET` fügt mehrere Schlüssel und Werte in Redis hinzu.

Syntax: `MSET key value [key value ...]`

Beispiel:

```
MSET name "Max" age 25 email "max@example.com"

```

Das obige Beispiel fügt die Schlüssel `name` mit dem Wert `"Max"`, `age` mit dem Wert `25` und `email` mit dem Wert `"max@example.com"` in Redis hinzu.

### MGET

Der Befehl `MGET` gibt die Werte mehrerer Schlüssel in Redis zurück.

Syntax: `MGET key [key ...]`

Beispiel:

```
MSET name "Max" age 25 email "max@example.com"
MGET name age email

```

Das obige Beispiel gibt die Werte der Schlüssel `name`, `age` und `email` zurück, also `"Max"`, `25` und `"max@example.com"`.

### DEL

Der Befehl `DEL` löscht einen Schlüssel und seinen Wert aus Redis.

Syntax: `DEL key`

Beispiel:

```
SET mykey "Hello Redis"
DEL mykey

```

Das obige Beispiel löscht den Schlüssel `mykey` und seinen Wert aus Redis. Wenn der Schlüssel nicht vorhanden ist, hat der Befehl keine Auswirkungen.

---

## Listen

### LPUSH

Der Befehl `LPUSH` fügt ein Element am Anfang einer Liste in Redis hinzu.

Syntax: `LPUSH key value`

Beispiel:

```
LPUSH mylist "world"
LPUSH mylist "hello"
```

### RPUSH

Der Befehl `RPUSH` fügt ein Element am Ende einer Liste in Redis hinzu.

Syntax: `RPUSH key value`

Beispiel:

```
RPUSH mylist "hello"
RPUSH mylist "world"
```

### LPOP

Der Befehl `LPOP` entfernt und gibt das erste Element aus einer Liste in Redis zurück.

Syntax: `LPOP key`

Beispiel:

```
LPUSH mylist "world"
LPUSH mylist "hello"
LPOP mylist
```

### RPOP

Der Befehl `RPOP` entfernt und gibt das letzte Element aus einer Liste in Redis zurück.

Syntax: `RPOP key`

Beispiel:

```
RPUSH mylist "hello"
RPUSH mylist "world"
RPOP mylist
```

### LRANGE

Der Befehl `LRANGE` gibt eine Teilmenge einer Liste in Redis zurück. Sie müssen einen Start- und einen Endindex angeben, um die Elemente auszuwählen, die Sie in der Liste anzeigen möchten.

Syntax: `LRANGE key start end`

Beispiel:

```
LPUSH mylist "world"
LPUSH mylist "hello"
LRANGE mylist 0 1
```

Das obige Beispiel gibt die ersten beiden Elemente in der Liste `mylist` zurück, also `"hello"` und `"world"`.

Um sich die gesamte Liste ausgeben zu lassen, geben Sie als Startindex 0 und als Endindex -1 an.

---

## Ablaufende Strings (Expiration)

### EXPIRE

Der Befehl `EXPIRE` legt die Lebensdauer eines Schlüssels in Redis fest und gibt an, wie lange der Schlüssel im Speicher bleiben soll, bevor er automatisch gelöscht wird.

Syntax: `EXPIRE key seconds`

Beispiel:

```
SET mykey "Hello Redis"
EXPIRE mykey 60

```

Das obige Beispiel legt die Lebensdauer des Schlüssels `mykey` auf 60 Sekunden fest. Nach Ablauf dieser Zeit wird der Schlüssel und sein Wert automatisch aus Redis gelöscht. Wenn Sie die Lebensdauer eines Schlüssels verlängern möchten, können Sie den Befehl `EXPIREAT` verwenden, der eine UNIX-Zeitstempel anstelle von Sekunden akzeptiert.

```
EXPIREAT mykey 1627992000

```

Dieses Beispiel legt die Lebensdauer des Schlüssels `mykey` auf den 3. August 2021 um 12:00 Uhr UTC fest. Nach Ablauf dieser Zeit wird der Schlüssel und sein Wert automatisch aus Redis gelöscht.

Es ist wichtig zu beachten, dass der Befehl `EXPIRE` nur auf Schlüssel angewendet werden kann, die bereits existieren. Wenn der Schlüssel nicht vorhanden ist, hat der Befehl keine Auswirkungen.

### SETEX

Der Befehl `SETEX` speichert einen Schlüssel mit einem bestimmten Wert und einer ablaufenden Lebensdauer in Redis.

Syntax: `SETEX key seconds value`

Beispiel:

```
SETEX mykey 60 "Hallo Redis"

```

Das obige Beispiel speichert den Schlüssel `mykey` mit dem Wert `"Hallo Redis"` und einer Lebensdauer von 60 Sekunden. Das bedeutet, dass der Schlüssel und sein Wert nach 60 Sekunden automatisch aus Redis gelöscht werden.


### TTL

Der Befehl `TTL` gibt die verbleibende Lebensdauer eines Schlüssels in Sekunden zurück. Wenn der Schlüssel nicht abläuft, gibt Redis `-1` zurück. Wenn der Schlüssel nicht vorhanden ist, gibt Redis `-2` zurück.

Syntax: `TTL key`

Beispiel:

```
SET mykey "Hello Redis"
EXPIRE mykey 60
TTL mykey

```

Das obige Beispiel speichert den Schlüssel `mykey` mit dem Wert `"Hello Redis"` und einer Lebensdauer von 60 Sekunden. Der Befehl `TTL mykey` gibt die verbleibende Lebensdauer des Schlüssels `mykey` in Sekunden zurück. Wenn der Schlüssel noch nicht abgelaufen ist, gibt Redis die verbleibende Zeit in Sekunden zurück. Wenn der Schlüssel nicht vorhanden ist, gibt Redis `-2` zurück.

---

## Sets

### SADD

Der Befehl `SET` speichert einen Wert unter einem Schlüssel in Redis. Im Unterschied zu einer Liste in Redis, können in einem Set keine Duplikate von Werten gespeichert werden.

Syntax: `SADD key member [member ...]`

Beispiel:

```
SADD myset "Hello"
SADD myset "World"
```

### SMEMBERS

Der Befehl `SMEMBERS` gibt alle Mitglieder eines Sets in Redis zurück.

Syntax: `SMEMBERS key`

Beispiel:

```
SADD myset "Hello"
SADD myset "World"
SMEMBERS myset
```

Das obige Beispiel gibt alle Mitglieder des Sets `myset` zurück, also `"Hello"` und `"World"`.

### SREM

Der Befehl `SREM` entfernt Mitglieder aus einem Set in Redis.

Syntax: `SREM key member [member ...]`

Beispiel:

```
SADD myset "Hello"
SADD myset "World"
SADD myset "Redis"
SREM myset "World"
```

Das obige Beispiel entfernt das Mitglied `"World"` aus dem Set `myset`. Das Set enthält nun nur noch die Mitglieder `"Hello"` und `"Redis"`.

---

## Hashes

Hashes sind wie ein JSON-Objekt, bestehend aus einem Schlüssel und einem oder mehreren Feldern und Werten. Ein Hash kann allerdings nicht verschachtelt werden.

Hashes sind in Redis wie JSON-Objekte aufgebaut, bestehend aus einem Schlüssel und einem oder mehreren Feldern und Werten. Im Gegensatz zu JSON-Objekten können Hashes jedoch nicht verschachtelt werden.

### HSET

Der Befehl `HSET` fügt einem Hash-Objekt ein Feld mit einem Wert hinzu.

Syntax: `HSET key field value`

Beispiel:

```
HSET user name "Max"
HSET user age 25
```

### HGET

Der Befehl `HGET` gibt den Wert eines Feldes in einem Hash-Objekt zurück.

Syntax: `HGET key field`

Beispiel:

```
HGET user name
```

### HGETALL

Der Befehl `HGETALL` gibt alle Felder und Werte eines Hash-Objekts zurück.

Syntax: `HGETALL key`

Beispiel:

```
HSET user name "Max"
HSET user age 25
HGETALL user
```

Das obige Beispiel gibt alle Felder und Werte des Hash-Objekts `user` zurück, also `"name"` und `"Max"` sowie `"age"` und `25`.

### HEXISTS

Der Befehl `HEXISTS` gibt zurück, ob ein Feld in einem Hash-Objekt existiert.

Syntax: `HEXISTS key field`

Beispiel:

```
HEXISTS user name
```

Das obige Beispiel gibt `1` zurück, wenn das Feld `name` im Hash-Objekt `user` vorhanden ist, andernfalls gibt es `0` zurück.

### HDEL

Der Befehl `HDEL` löscht ein Feld in einem Hash-Objekt.

Syntax: `HDEL key field`

Beispiel:

```
HDEL user age
```

Das obige Beispiel löscht das Feld `age` aus dem Hash-Objekt `user`.

---

# Weitere Befehle für Redis

## Daten mit Lebensdauer

### TTL

Der Befehl `TTL` gibt die verbleibende Lebensdauer eines Schlüssels in Sekunden zurück. Wenn der Schlüssel nicht abläuft, gibt Redis `-1` zurück. Wenn der Schlüssel nicht vorhanden ist, gibt Redis `-2` zurück.

Syntax: `TTL key`

Beispiel:

```
SET mykey "Hello Redis"
EXPIRE mykey 60
TTL mykey

```

Das obige Beispiel gibt die verbleibende Lebensdauer des Schlüssels `mykey` zurück, der auf 60 Sekunden eingestellt ist.

### SETEX

Der Befehl `SETEX` speichert einen Schlüssel mit einem bestimmten Wert und einer ablaufenden Lebensdauer in Redis.

Syntax: `SETEX key seconds value`

Beispiel:

```
SETEX mykey 60 "Hello Redis"

```

Das obige Beispiel speichert den Schlüssel `mykey` mit dem Wert `"Hello Redis"` und einer Lebensdauer von 60 Sekunden.

### FLUSHALL

Der Befehl `FLUSHALL` löscht alle Schlüssel und ihre Werte aus allen Datenbanken in Redis. Achtung: Dieser Befehl kann nicht rückgängig gemacht werden.

Syntax: `FLUSHALL`

Beispiel:

```
SET mykey "Hello Redis"
FLUSHALL
```

Das obige Beispiel löscht den Schlüssel `mykey` und alle anderen Schlüssel und Werte in Redis.
